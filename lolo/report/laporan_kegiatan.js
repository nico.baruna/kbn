frappe.query_reports["Laporan Kegiatan"] = {
	"filters": [
        {
			"fieldname":"report_date",
			"label": __("As on Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.get_today()
		},
        ]
    } 