from __future__ import unicode_literals
import frappe
#from frappe.utils import date_diff, nowdate, format_date, add_days
import requests
import json
from pprint import pprint
from datetime import datetime, timedelta
#from var_dump import var_dump

  
def every_minute() :
    url = "http://10.8.3.34/service/" 

    myResponse = requests.get(url)
    if(myResponse.ok):
        jData = json.loads(myResponse.content) 
        #result = frappe.get_last_doc("lolo movement")
        # lastRecordDate = result.start_time.strftime("%Y-%m-%d %H:%M:%S");
        result =  frappe.db.sql("select MAX(start_time) from `tablolo movement` ")
        lastRecordDate = datetime.strptime("2018-11-05 00:00:00", "%Y-%m-%d %H:%M:%S")
       
        if(result[0][0] != None):
            lastRecordDate = result[0][0]

        if(len(jData['count'])):
           
           for data in jData['data_array'] :
               print(data['start_time'][:19])
               if(data['start_time']):
                    dataDate = datetime.strptime(data['start_time'][:19], "%Y-%m-%d %H:%M:%S")
               
               if(dataDate > lastRecordDate):
                    record = frappe.get_doc({
                        "doctype":"lolo movement", 
                        "asset_no": data['asset_no'],
                        "start_lat":  data['start_lat'],
                        "start_lon":  data['start_lon'],
                        "start_location":  data['start_location'],
                        "stop_lat":  data['stop_lat'],
                        "stop_lon":  data['stop_lon'],
                        "stop_location":  data['stop_location'],
                        "distance_travelled":  data['distance_travelled'],
                        "journey_duration":  data['journey_duration'],
                        "start_time": data['start_time'][:19],
                        "stop_time": data['stop_time'][:19]
                
                    })
                    record.insert(ignore_permissions = True)
                    print("data Stored")
            
    else :
        myResponse.raise_for_status()
