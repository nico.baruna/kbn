// Copyright (c) 2018, ILCS and contributors
// For license information, please see license.txt

frappe.ui.form.on('tes', {
	refresh: function(frm) {
		
	frm.add_custom_button("Send SMS", function(click){
		var mobile_number_list=[];
		var number_list=[];
		var mobile=cur_frm.doc.mobile_number;
		// function to fetch only from current document
		frappe.call({
			method: 'frappe.client.get_list',
			args:
			{
				'doctype': 'Lead',
				'fields': ['mobile_number','phone_number'],
				filters:
				{
					name:frm.doc.name,
				}
			},
			async: false,
			callback: function(r)
			{
				number_list= r.message;
				console.log(number_list);
				console.log(number_list.length);
				for (var i = 0; i <= number_list.length; i++) {
					mobile_number_list[i]= number_list[i].mobile_number;
				}
			}
		});
		
		// Function to show dialog box
		var d = new frames.ui.dialog({
			'fields': [
				{"fieldtype": "Select", "label": "Mobile Number", "fieldname": "mobile_number","options":mobile_number_list},
				
			],
			primary_action: function(){
				d.hide();
				show_alert(d.get_values());
			}
		});
		
		d.show;
	});
		
	}
});

