// Copyright (c) 2018, ILCS and contributors
// For license information, please see license.txt

frappe.ui.form.on('spk_order', {
	refresh: function(frm) {
		console.log('test');
		
	}
});

frappe.ui.form.on('Truck Association', {
	onload: function(frm) {
		console.log('test Nico');
		
	}
});

frappe.ui.form.on("Truck Association", "nomor_container", function(frm, cdt, cdn) {
	frappe.call({
		"method" : "frappe.client.get",
		args : {
			doctype: "Master Container",
			'filters': {'name': frm.doc.no_container},
		},
		callback : function(data){
			var item  = frappe.get_doc(cdt, cdn);
			console.log(item.doctype+' '+item.name);
			frappe.model.set_value(item.doctype,item.name,'size',data.message.size)
			frappe.model.set_value(item.doctype,item.name,'type',data.message.type)
			frappe.model.set_value(item.doctype,item.name,'status',data.message.status)
		}
	})
	
})

frappe.ui.form.on("Truck Association", "truck_id", function(frm, cdt, cdn) {
	frappe.call({
		"method" : "frappe.client.get",
		args : {
			doctype: "Truck",
			'filters': {'name': frm.doc.truck_id},
		},
		callback : function(data){
			var item  = frappe.get_doc(cdt, cdn);
			console.log(item.doctype+' '+item.name);
			frappe.model.set_value(item.doctype,item.name,'type_truck',data.message.type)
			frappe.model.set_value(item.doctype,item.name,'angkutan',data.message.angkutan)
		}
	})
	
})

frappe.ui.form.on("Truck Association", "done", function(frm, cdt, cdn) {
	var item  = frappe.get_doc(cdt, cdn);
	console.log(frappe.datetime)
	frappe.model.set_value(item.doctype,item.name,'done_date',frappe.datetime.now_datetime())
	frappe.model.set_value(item.doctype,item.name,'movement_status',"done")
	
	
})

frappe.ui.form.on("Truck Association", "refresh", function(frm, cdt, cdn) {
	console.log("ihiiiyy")
})

