frappe.pages['tes'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Test Page',
		single_column: true
	});
	
	console.log(page);
	// Chart constructor works like this:
	// const your-chart
	// Parameters required are:
	// #1 container element =  This is any CSS selector or DOM Object
	// #2 options object = At a minimum:  Data, composed itself of an array of labels and array of datasets
	const container = '.layout-main-section';
	// .layout-main-section
	const options = {
		parent: $(page.container),
		data: {
			labels: ["12am-3am", "3am-6pm", "6am-9am", "9am-12am",
				"12pm-3pm", "3pm-6pm", "6pm-9pm", "9am-12am"
			],

			datasets: [
				{
					name: "Some Data", type: "bar",
					values: [25, 40, 30, 35, 8, 52, 17, -4]
				},
				{
					name: "Another Set", type: "line",
					values: [25, 50, -10, 15, 18, 32, 27, 14]
				}
			]
		},
		title: "My Awesome Chart on a page in ERPNext",
		type: 'axis-mixed', // or 'bar', 'line', 'scatter', 'pie', 'percentage'
		height: 250,
		colors: ['#7cd6fd', '#743ee2']
	}

	const chart = new Chart(container, options)
	setTimeout(function () {chart.draw(!0)}, 1);
}