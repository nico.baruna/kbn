

function getData(){
	var data = '';
	frappe.call({
		method: 'lolo.lolo.page.dashboard.dashboard.getRsActivity',
		async : false,
		callback: function(r) {
			
			if (!r.exc) {
				// code snippet
				console.log(r.message) 
				data = r.message	
			}
		}
	});
	return data;
}

function getLastPos(){
    var dataReturn;
    // this will generate another thread to run in another function
    jQuery.ajax({
        url: 'http://172.16.254.51/kbnservice/getPosition.php',
        type: 'get',
        dataType: 'json',
        async : false,
        success: function(data) {
            dataReturn = data;
        }, 
        error: function() {
           console.log("error")
        }
	});
	
	console.log(dataReturn.data_array[0]);

    return dataReturn.data_array[0];
}

function getUsage(){
	var data = '';
	frappe.call({
		method: 'lolo.lolo.page.dashboard.dashboard.getUsage',
		async : false,
		callback: function(r) {
			
			if (!r.exc) {
				// code snippet
				console.log(r.message) 
				data = r.message	
			}
		}
	});
	return data;
}

function getMovement(){
	var data = '';
	frappe.call({
		method: 'lolo.lolo.page.dashboard.dashboard.getAvgMovement',
		async : false,
		callback: function(r) {
			
			if (!r.exc) {
				// code snippet
				console.log(r.message) 
				data = r.message	
			}
		}
	});
	return data;
}

frappe.pages['dashboard'].on_page_load = function(wrapper) {
	
	new MyPage(wrapper);

	//get Active Time
	var activeTime = getActiveTime()
	$('#activeTime').html(activeTime.active_time)

	//getAvg Mvmt
	var avgMovement =  getMovement()
	$('#avgMovement').html(avgMovement.rata)

	//get activity
	const container = '#fuelUsage';
	var data = getData()
	// .layout-main-section
	const options = {
		parent: $(container),
		data: {
			labels: data.dates,

			datasets: [
				{
					name: "Movement", type: "bar",
					values: data.move
				},
				
				{
					name: "Duration", type: "bar",
					values: data.duration
				}
			]
		},
		title: "Pergerakan Vs Durasi",
		type: 'bar', // or 'bar', 'line', 'scatter', 'pie', 'percentage'
		height: 250,
		colors: ['#7cd6fd', '#743ee2']
	}

	const chart = new Chart(container, options)
	setTimeout(function () {chart.draw(!0)}, 1);
	//map section

	
	//var data = getLastPos();
	var mymap = L.map('map').setView([-6.107632, 106.875491], 13);
	var markers = new L.LayerGroup().addTo(mymap);
	
	  
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
			maxZoom: 18,
			attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
				'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
				'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
			id: 'mapbox.streets'
		}).addTo(mymap);

	var myIcon = L.icon({
		iconUrl: 'marker.png',
		iconSize: [35, 35],
	});
}

MyPage = Class.extend({
	init: function(wrapper) {
		this.page = frappe.ui.make_app_page({
			parent: wrapper,
			title: 'Dashboard',
			single_column: true
		});
		this.make();
	},
	make: function() {
		$(frappe.render_template("dashboard", this)).appendTo(this.page.main);
	}

})


function getActiveTime(){
	var data = '';
	frappe.call({
		method: 'lolo.lolo.page.dashboard.dashboard.getActiveTime',
		async : false,
		callback: function(r) {
			
			if (!r.exc) {
				// code snippet
				console.log(r.message) 
				data = r.message	
			}
		}
	});
	return data;
}


function getAvgMovement(){
	var data = '';
	frappe.call({
		method: 'lolo.lolo.page.dashboard.dashboard.getAvgMovement',
		async : false,
		callback: function(r) {
			
			if (!r.exc) {
				// code snippet
				console.log(r.message) 
				data = r.message	
			}
		}
	});
	return data;
}

function getRsActivity(){
	var data = '';
	frappe.call({
		method: 'lolo.lolo.page.dashboard.dashboard.getRsActivity',
		async : false,
		callback: function(r) {
			
			if (!r.exc) {
				// code snippet
				console.log(r.message) 
				data = r.message	
			}
		}
	});
	return data;
}