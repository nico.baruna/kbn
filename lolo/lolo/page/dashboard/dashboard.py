# -*- coding: utf-8 -*-
# Copyright (c) 2017-2018, libracore and contributors
# License: AGPL v3. See LICENCE

from __future__ import unicode_literals
import frappe
from frappe import throw, _
import json 


@frappe.whitelist()
def getData():
	data = frappe.get_all("lolo movement",filters={'status': '1'}, fields=['asset_no', 'ts','status'])
	return data

@frappe.whitelist()
def getActiveInactive():
	#data = frappe.db.sql("select round(count(*)*60) as active , TIMESTAMPDIFF(HOUR, DATE_FORMAT(NOW() ,'%Y-%m-01'), NOW()) as `difference` from `tablolo movement` WHERE MONTH(ts) = MONTH(CURRENT_DATE()); ")
	data = frappe.db.sql("select concat(floor(HOUR( sec_to_time(round(count(*)*60)))/24) , ' days',mod(HOUR( sec_to_time(round(count(*)*60))), 24) , ' hours', MINUTE( sec_to_time(round(count(*)*60))), ' minutes')  as active , TIMESTAMPDIFF(HOUR, DATE_FORMAT(NOW() ,'%Y-%m-01'), NOW()) as `difference` from `tablolo movement` WHERE MONTH(ts) = MONTH(CURRENT_DATE()); ")
	#inactive = data[0][1] - data[0][0]
	#GetTime(data[0][0])
	inactive = data[0][1] - data[0][0]
	returnData = {'active':data[0][0],"inactive":inactive}
	return returnData


@frappe.whitelist()
def replaceNull(x):
  if x == None:
    return 0
  else:
    return x

@frappe.whitelist()
def getUsage():
	#data = frappe.db.sql("SELECT * FROM (SELECT COUNT(DISTINCT MINUTE(ts)) as menit, HOUR(ts) as jam FROM `tablolo movement` WHERE DATE_FORMAT(ts, '%Y-%m-%d') = CURRENT_DATE() GROUP BY HOUR(ts)) AS a RIGHT JOIN (SELECT DISTINCT(HOUR(DATE(NOW()) + INTERVAL (seq * 60) MINUTE)) AS jam_now FROM seq_0_to_24) AS b ON a.jam = b.jam_now ORDER BY jam_now ASC; ")
	data  = frappe.db.sql("select round(count(*)/60) as active , DAY(ts) as `date` from `tablolo movement` WHERE MONTH(ts) = MONTH(CURRENT_DATE()) group by DAY(ts)")
	result = data
	dates = []
	active = []
	for row in result:
		dates.append({row[1]})
	for row in result:
		active.append({row[0]})
	
	returnData = {'dates': dates, 'active': active}
	return returnData

@frappe.whitelist()
def getMovement():
	data = frappe.db.sql("select count(*) as movement , day(ts) as day, a_id as status from `tablolo movement`  WHERE MONTH(ts) = MONTH(CURRENT_DATE()) and (a_id = 2) group by day(ts) , a_id;")
	result = []
	datesList = []
	movement2 = []
	movement3 = []
	prevDates = 0
	for row in data:
		if(row[2]=='2'):
			movement2.append({row[0]})
		else :
			movement3.append({row[0]})

		if(prevDates != row[1]):
			datesList.append({row[1]})
		prevDates = row[1]
		
	result = {"date":datesList,"up":movement2,"down":movement3}

	return result

@frappe.whitelist()
def getActiveTime():
	data = frappe.db.sql("select sec_to_time(sum(journey_duration)) as active_time  from `tablolo movement`; ")
	returnData = {'active_time':data[0][0]}
	return returnData

@frappe.whitelist()
def getAvgMovement():
	data = frappe.db.sql("select sec_to_time((sum(distance_travelled)/ count(distance_travelled))) as rata from `tablolo movement`; ")
	returnData = {'rata':data[0][0]}
	return returnData

@frappe.whitelist()
def getRsActivity():
	data  = frappe.db.sql("select count(*) as pegerakan, sum(distance_travelled) as distance, sum(journey_duration)/60 as meter, date(start_time) as tanggal from `tablolo movement` group by date(start_time); ")
	result = data
	move = []
	duration = []
	dates = []
	distance = []
	for row in result:
		move.append({row[0]})
	for row in result:
		duration.append({row[2]})
	for row in result:
		distance.append({row[1]})
	for row in result:
		dates.append({row[3]})
	
	returnData = {'dates': dates, 'move': move, 'duration': duration,'distance':distance}
	return returnData
