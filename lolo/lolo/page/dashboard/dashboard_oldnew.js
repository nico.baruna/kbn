
frappe.pages['dashboard'].on_page_load = function(wrapper) {
	new MyPage(wrapper);
	frappe.breadcrumbs.add("Lolo")
	//MyPage.add_menu_item('spk_order', () => frappe.set_route('List', 'spk_order'))
	
	// Chart constructor works like this:
	// const your-chart
	// Parameters required are:
	// #1 container element =  This is any CSS selector or DOM Object
	// #2 options object = At a minimum:  Data, composed itself of an array of labels and array of datasets
	const container = '#totalMovemen';
	const container2 = '#fleetPerformance';
	// .layout-main-section
	const options = {
		parent: $(container),
		data: {
			labels: ["12am-3am", "3am-6pm", "6am-9am", "9am-12am",
				"12pm-3pm", "3pm-6pm", "6pm-9pm", "9am-12am"
			],

			datasets: [
				{
					name: "Some Data", type: "pie",
					values: [25, 40, 30, 35, 8, 52, 17, -4]
				},
				{
					name: "Another Set", type: "pie",
					values: [25, 50, -10, 15, 18, 32, 27, 14]
				}
			]
		},
		title: "My Awesome Chart on a page in ERPNext",
		type: 'bar', // or 'bar', 'line', 'scatter', 'pie', 'percentage'
		height: 250,
	
		colors: ['#7cd6fd', '#743ee2']
	}
	
	/**
	 * Chart untuk performance
	 */

	const pieoptions = {
		parent: $(container2),
		data: {
			labels: ["active","Inactive","others"],
		
			datasets: [
				{
					name: "Some Data", 
					values: [70, 20,10]
				},
				
				
			]

			},
		
			title: "My Awesome Chart",
			type: 'bar', // or 'bar', 'line', 'pie', 'percentage'
			height: 300,
			colors: ['purple', '#ffa3ef', 'light-blue'],
		
			
	}

	const chart = new Chart(container, options)
	const piechart = new Chart(container2, pieoptions)
	setTimeout(function () {chart.draw(!0)}, 1);
	setTimeout(function () {piechart.draw(!0)}, 1);
	
}

MyPage = Class.extend({
	init: function(wrapper) {
		this.page = frappe.ui.make_app_page({
			parent: wrapper,
			title: 'Dashboard',
			single_column: true
		});
		this.make();
	},
	make: function() {
		$(frappe.render_template("dashboard", this)).appendTo(this.page.main);
	}
})


